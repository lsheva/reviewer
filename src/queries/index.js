import gql from "graphql-tag";

export const MESSAGES_QUERY = gql`
  query messagesQuery($orderBy: MessagesOrderByInput, $filter: String, $first: Int, $skip: Int) {
    messages(orderBy: $orderBy, filter: $filter, first: $first, skip: $skip) {
      count
      messagesList {
        id
        text
        likes
        dislikes
        replies {
          id
          text
          likes
          dislikes
          isReplyTo {
            id
          }
        }
      }
    }
  }
`;

export const REPLIES_QUERY = gql`
  query repliesQuery($orderBy: MessagesOrderByInput) {
    replies(orderBy: $orderBy) {
      id
      text
      likes
      dislikes
      isReplyTo {
        id
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation MessagesMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const POST_REPLY_MUTATION = gql`
  mutation ReplyMutation($text: String!, $isReplyToId: ID!) {
    postReply(text: $text, isReplyToId: $isReplyToId) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const LIKE_MUTATION = gql`
  mutation LikeMutation($id: ID!) {
    like(id: $id) {
      id
      text
      likes
      dislikes
    }
  }
`;
export const DISLIKE_MUTATION = gql`
  mutation DislikeMutation($id: ID!) {
    dislike(id: $id) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const LIKE_REPLY_MUTATION = gql`
  mutation LikeReplyMutation($id: ID!) {
    likeReply(id: $id) {
      id
      text
      likes
      dislikes
    }
  }
`;
export const DISLIKE_REPLY_MUTATION = gql`
  mutation DislikeReplyMutation($id: ID!) {
    dislikeReply(id: $id) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const NEW_REPLIES_SUBSCRIPTION = gql`
  subscription {
    newReply {
      id
      text
      likes
      dislikes
      isReplyTo {
        id
      }
    }
  }
`;
