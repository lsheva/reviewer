import React, { useState, useEffect } from "react";
import { Query } from "react-apollo";
import { MESSAGES_QUERY, REPLIES_QUERY, NEW_MESSAGES_SUBSCRIPTION, NEW_REPLIES_SUBSCRIPTION } from "../../queries";
import "./Chat.scss";

import InfiniteScroll from "react-infinite-scroller";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";

import Message from "./Message";
import MessageInput from "./MessageInput";

const Chat = props => {
  const orderByOptions = ["createdAt_DESC", "createdAt_ASC", "likes_DESC", "likes_ASC", "dislikes_DESC", "dislikes_ASC"];
  const [orderBy, setOrderBy] = useState(orderByOptions[0]);
  const [filter, setFilter] = useState("");
  const first = 7;
  const skip = 0;
  let chatRef = React.createRef();

  const scrollDownMessageList = () => {
    const elem = chatRef.current;
    elem.scrollTop = elem.scrollHeight;
  };

  // aka componentDidMount
  useEffect(() => {
    scrollDownMessageList();
  }, []);

  const _subscribeToNewMessages = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGES_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messagesList.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return {
          ...prev,
          messages: {
            messagesList: [newMessage, ...prev.messages.messagesList],
            count: prev.messages.messagesList.length + 1,
            __typename: prev.messages.__typename
          }
        };
      }
    });
  };

  const _subscribeToNewReplies = subscribeToMore => {
    subscribeToMore({
      document: NEW_REPLIES_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newReply } = subscriptionData.data;

        const newMessagesList = prev.messages.messagesList.map(message => {
          if (message.id !== newReply.isReplyTo.id) return message;

          const newReplies = message.replies.map(reply => {
            if (reply.id === newReply.id) return newReply;
            return reply;
          });

          const index = message.replies.findIndex(reply => reply.id === newReply.id);

          if (index === -1) {
            return { ...message, replies: [...message.replies, newReply] };
          } else {
            const newMessage = { ...message };
            newMessage.replies[index] = newReply;
            return newMessage;
          }
        });

        return {
          ...prev,
          messages: {
            messagesList: newMessagesList,
            count: newMessagesList.length,
            __typename: prev.messages.__typename
          }
        };
      }
    });
  };

  const _replyToMessageSubmit = (store, newReply, messageId) => {
    // const orderBy = "createdAt_ASC";
    // console.log(store);
    // const data = store.readQuery({
    //   query: REPLIES_QUERY
    // });
    // data.messages.messagesList.find(message => message.id === messageId).replies.push(newReply);
    // store.writeQuery({
    //   query: MESSAGES_QUERY,
    //   data
    // });
  };

  const _onSortUpdate = ({ value }) => {
    setOrderBy(value);
  };

  const _onFilterUpdate = e => {
    setFilter(e.target.value);
  };

  return (
    <React.Fragment>
      <div className="header">
        <div className="sitename">Superchat</div>
        <div className="search">
          <input type="text" onChange={_onFilterUpdate} value={filter} placeholder="Live search..." />
        </div>
        <div className="dropdown">
          <Dropdown options={orderByOptions} onChange={_onSortUpdate} value={orderBy} />
        </div>
      </div>
      <div className="Chat" ref={chatRef}>
        <Query query={MESSAGES_QUERY} variables={{ first, skip, orderBy, filter }}>
          {({ loading, error, data, subscribeToMore, fetchMore, variables }) => {
            if (loading) return <div>Loading...</div>;
            if (error) return <div>Fetch error</div>;
            _subscribeToNewMessages(subscribeToMore);
            _subscribeToNewReplies(subscribeToMore);

            const _onLoadMore = () => {
              fetchMore({
                variables: {
                  skip: data.messages.messagesList.length
                },
                updateQuery: (prev, { fetchMoreResult }) => {
                  if (!fetchMoreResult) {
                    return prev;
                  }
                  return {
                    ...prev,
                    messages: {
                      messagesList: [...prev.messages.messagesList, ...fetchMoreResult.messages.messagesList],
                      count: prev.messages.count,
                      __typename: prev.messages.__typename
                    }
                  };
                }
              });
            };
            const hasMore = data.messages.count > data.messages.messagesList.length;
            console.log(hasMore);
            console.log(data);
            return (
              <React.Fragment>
                <InfiniteScroll
                  pageStart={0}
                  loadMore={() => _onLoadMore()}
                  hasMore={hasMore}
                  isReverse={true}
                  loader={
                    <div key="_loader" className="loader">
                      Loading ...
                    </div>
                  }
                  useWindow={false}
                >
                  {data.messages.messagesList
                    .slice(0)
                    .reverse()
                    .map((item, index) => (
                      <Message key={item.id} index={index} {...item} replyToMessageSubmit={_replyToMessageSubmit} />
                    ))}
                </InfiniteScroll>
              </React.Fragment>
            );
          }}
        </Query>
      </div>
      <MessageInput />
    </React.Fragment>
  );
};

export default Chat;
