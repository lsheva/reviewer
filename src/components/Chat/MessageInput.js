import React from "react";
import { Mutation } from "react-apollo";
import { POST_MESSAGE_MUTATION } from "../../queries";

class MessageInput extends React.Component {
  state = {
    text: ""
  };

  componentDidMount() {
    const { text } = this.props;
    text && this.setState({ text });
  }

  onMessageSubmitCompleted = () => {
    this.setState({ text: "" });
  };

  onEnterPress = (e, postMutation) => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      return postMutation();
    }
  };

  render() {
    return (
      <div className="MessageInput">
        <Mutation mutation={POST_MESSAGE_MUTATION} variables={{ text: this.state.text }} onCompleted={this.onMessageSubmitCompleted}>
          {postMutation => (
            <>
              <textarea
                value={this.state.text}
                onKeyDown={e => this.onEnterPress(e, postMutation)}
                onChange={e => this.setState({ text: e.target.value })}
              />
              <button className="submitButton" onClick={postMutation}>
                Submit
              </button>
            </>
          )}
        </Mutation>
      </div>
    );
  }
}

export default MessageInput;
