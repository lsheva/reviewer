import React from "react";
import { Mutation } from "react-apollo";
import { LIKE_REPLY_MUTATION, DISLIKE_REPLY_MUTATION } from "../../queries";

const Reply = props => {
  const { text, id, index, likes, dislikes } = props;

  return (
    <div className="Reply messageClass">
      <div className="content">
        <div className="text">{text}</div>
        <div className="id right">#{index}</div>
      </div>
      <div className="actions">
        <Mutation mutation={LIKE_REPLY_MUTATION} variables={{ id, type: "REPLY" }}>
          {likeMutation => (
            <div className="action" onClick={likeMutation}>
              Like {likes}
            </div>
          )}
        </Mutation>
        <Mutation mutation={DISLIKE_REPLY_MUTATION} variables={{ id }}>
          {dislikeMutation => (
            <div className="action" onClick={dislikeMutation}>
              Dislike {dislikes}
            </div>
          )}
        </Mutation>
      </div>
    </div>
  );
};

export default Reply;
