import React from "react";
import { Mutation } from "react-apollo";
import { POST_REPLY_MUTATION } from "../../queries";

class ReplyInput extends React.Component {
  state = {
    text: ""
  };

  componentDidMount() {
    const { text } = this.props;
    text && this.setState({ text });
  }

  render() {
    return (
      <div className="MessageInput">
        <textarea value={this.state.text} onChange={e => this.setState({ text: e.target.value })} />
        <Mutation
          mutation={POST_REPLY_MUTATION}
          variables={{ text: this.state.text, isReplyToId: this.props.isReplyToId }}
          update={(store, { data: { postReply } }) => {
            this.props.replyToMessageSubmit(store, postReply, this.props.isReplyToId);
          }}
          onCompleted={() => {
            this.setState({ text: "" });
            this.props.replyToMessageCompleted();
          }}
        >
          {replyMutation => (
            <button className="submitButton" onClick={replyMutation}>
              Submit
            </button>
          )}
        </Mutation>
      </div>
    );
  }
}

export default ReplyInput;
