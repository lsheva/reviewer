import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { LIKE_MUTATION, DISLIKE_MUTATION } from "../../queries";

import Reply from "./Reply";
import ReplyInput from "./ReplyInput";

const Message = props => {
  const { text, id, likes, dislikes, replies } = props;
  const [replyActive, setReplyActive] = useState(false);

  const replyToMessage = () => {
    setReplyActive(true);
  };

  const replyToMessageCompleted = () => {
    setReplyActive(false);
  };

  return (
    <div className="Message messageClass">
      <div className="content">
        <div className="text">{text}</div>
        <div className="id right">#{id.substr(-4)}</div>
      </div>
      <div className="actions">
        <Mutation mutation={LIKE_MUTATION} variables={{ id }}>
          {likeMutation => (
            <div className="action" onClick={likeMutation}>
              Like {likes}
            </div>
          )}
        </Mutation>
        <Mutation mutation={DISLIKE_MUTATION} variables={{ id }}>
          {dislikeMutation => (
            <div className="action" onClick={dislikeMutation}>
              Dislike {dislikes}
            </div>
          )}
        </Mutation>
        <div className="action right">
          <button onClick={replyToMessage}>Reply</button>
        </div>
      </div>
      {replies && replies.map((reply, index) => <Reply key={reply.id} index={index} {...reply} />)}
      {replyActive && (
        <ReplyInput isReplyToId={id} replyToMessageSubmit={props.replyToMessageSubmit} replyToMessageCompleted={replyToMessageCompleted} />
      )}
    </div>
  );
};

export default Message;
