async function isReplyTo(parent, args, context) {
  const res = await context.prisma
    .reply({
      id: parent.id
    })
    .isReplyTo();
  return res;
}

module.exports = {
  isReplyTo
};
