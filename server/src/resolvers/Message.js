async function replies(parent, args, context) {
  const res = await context.prisma
    .message({
      id: parent.id
    })
    .replies();
  //console.log(res);
  return res;
}

module.exports = {
  replies
};
