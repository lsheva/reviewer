function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    text: args.text
  });
}

async function postReply(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.isReplyToId
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.isReplyToId} does not exist`);
  }

  return context.prisma.createReply({
    text: args.text,
    isReplyTo: { connect: { id: args.isReplyToId } }
  });
}

async function like(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.id
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.isReplyToId} does not exist`);
  }

  const where = { id: args.id };
  let likes = await context.prisma.message(where).likes();
  if (!likes) {
    likes = 0;
  }
  const data = { likes: likes + 1 };
  return context.prisma.updateMessage({ where, data });
}

async function dislike(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.id
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.isReplyToId} does not exist`);
  }

  const where = { id: args.id };
  let dislikes = await context.prisma.message(where).dislikes();
  if (!dislikes) {
    dislikes = 0;
  }
  const data = { dislikes: dislikes + 1 };
  return context.prisma.updateMessage({ where, data });
}

async function likeReply(parent, args, context, info) {
  const replyExists = await context.prisma.$exists.reply({
    id: args.id
  });

  if (!replyExists) {
    throw new Error(`reply with ID ${args.isReplyToId} does not exist`);
  }

  const where = { id: args.id };
  let likes = await context.prisma.reply(where).likes();
  if (!likes) {
    likes = 0;
  }
  const data = { likes: likes + 1 };
  return context.prisma.updateReply({ where, data });
}

async function dislikeReply(parent, args, context, info) {
  const replyExists = await context.prisma.$exists.reply({
    id: args.id
  });

  if (!replyExists) {
    throw new Error(`Reply with ID ${args.isReplyToId} does not exist`);
  }

  const where = { id: args.id };
  let dislikes = await context.prisma.reply(where).dislikes();
  if (!dislikes) {
    dislikes = 0;
  }
  const data = { dislikes: dislikes + 1 };
  return context.prisma.updateReply({ where, data });
}

module.exports = {
  postMessage,
  postReply,
  like,
  dislike,
  likeReply,
  dislikeReply
};
